# siam-pp-2020

This repository hosts the slides for the "Novel Computational Algorithms for Future Computing Platforms" mini-symposium held at SIAM Conference on Parallel Processing (SIAM PP20), Friday and Saturday, 14-15 February 2020.

**Organizers:** Arash Fathi, Dimitar Trenev, Laurent White, (ExxonMobil Corporate Strategic Research), Jason Riedy, Jeffrey Young (Directors of the CRNCH Rogues Gallery)

You can download all the available presentations as a zipfile [here](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/siam-pp-novel-comp-algorithms-future-computing-2020.zip).

### [MS51](https://meetings.siam.org/sess/dsp_programsess.cfm?SESSIONCODE=67837)

* [Scientific Computing in a Changing Landscape](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/01_white_sci_computing_siam_pp_20.pdf), Laurent White, Dimitar Trenev, Arash Fathi, ExxonMobil Research and Engineering
* [Data Movement Orchestration in Accelerator-Rich Systems](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/02_gertslauer_data_movement_siam_pp_2020.pdf), Andreas Gerstlauer, University of Texas at Austin
* Codesign Tradeoffs for Deep Learning Hardware Accelerators, Ardavan Pedram, Stanford University
* A Wafer-Scale Chip and System for Deep Neural Networks, Rob Schreiber, Cerebras

### [MS62](https://meetings.siam.org/sess/dsp_programsess.cfm?SESSIONCODE=67838)

* Quantum Computing - Overview & Potential Applications, Gilad Ben-Shach, IBM
* Towards Optical Neural Networks and Annealing Machines at the Quantum Limit, Ryan Hamerly, Alexander Sludds, Liane Bernstein, Marin Soljacic, and Dirk Englund, Massachusetts Institute of Technology
* [Quantum Circuit Synthesis using Linear Algebra and Optimization Algorithms](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/07_baboulin_quantum_circuit_synthesis_siam_pp_2020.pdf), Marc Baboulin, University of Paris-Sud, France
* [Neuromorphic Computing: A Platform for Machine Learning and Beyond](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/08_schuman_neuromorphic_siam_pp_2020.pdf), Catherine D. Schumann, Oak Ridge National Laboratory

### [MS72](https://meetings.siam.org/sess/dsp_programsess.cfm?SESSIONCODE=67839)

* [Leveraging Random Walks and Neuromorphic Hardward to Solve Elliptical Integro-PDEs](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/09_smith_random_walk_siam_pp_2020.pdf), Brad Aimone and J. Darby Smith, Sandia National Laboratories
* [Adaptive Local Timestepping and its Parallelization](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/10_bremer_adaptive_timestepping_siam_pp_2020.pdf), Max Bremer, Clint Dawson, University of Texas at Austin; John Bachan, Lawrence Berkeley National Laboratory; Cy Chan, Massachusetts Institute of Technology
* [The Rogues Gallery as a Testbed for Novel Algorithm Design for Future Architectures](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/11_young_rogues_gallery_siam_pp_2020.pdf), Jeffrey Young, Jason Riedy, Thomas M. Conte, Vivek Sarkar, Georgia Institute of Technology
* [Design of New Streaming and Graph Analytics Algorithms for the Strider Architecture](https://gitlab.com/crnch-rg/siam-pp-2020/-/blob/master/12_srikanth_strider_siam_pp_2020.pdf), Sriseshan Srikanth, Thomas M. Conte, Georgia Institute of Technology